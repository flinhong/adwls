# ----White Lists-------------------------------------------------------


# ----Blocks-----------------------------------------------------------

# Tuxera NTFS
||tuxera.com^

# 电视监控
# https://www.v2ex.com/t/772523
||gz-data .com^
||gzads.com^
||gozendata.com^

# 秒针广告
||e.cn.miaozhen.com^

# Adobe
||lcs1-cops.adobe.io
||p13n.adobe.io
||crs.cr.adobe.com
||lcs-cops.adobe.io
